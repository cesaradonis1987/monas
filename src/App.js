import React from 'react';
import './App.css';
import Navbar from './componentes/Navbar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Fotter from './componentes/Fotter';

//rutas
import Inicio from './componentes/pages/Inicio'
import Servicios from './componentes/pages/Servicios'
import Productos from './componentes/pages/Productos'
import Contacto from './componentes/pages/Contacto'
import Login from './componentes/pages/Login';
function App() {
  return (
    <>
    <Router>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Inicio}/>
        <Route path='/Servicios' exact component={Servicios}/>
        <Route path='/Productos' exact component={Productos}/>
        <Route path='/Contacto' exact component={Contacto}/>
        <Route path='/login' exact component={Login}/>
      </Switch>
      
    </Router>

    <Fotter />
</>
  );
}

export default App;
