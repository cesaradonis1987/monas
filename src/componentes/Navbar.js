import React, {useState} from 'react';
import {Button} from './Button';
import {Link} from 'react-router-dom';
import './Navbar.css';
import Dropdown from './Dropdown';


function Navbar (){

    const [click, setClick] = useState(false)
    const [dropdown, setDropdown] = useState(false)

    const handleClick=()=> setClick (!click)
    const closeMobilMenu=()=> setClick(false)

    const onMouseEnter=( )=> {
        if(window.innerWidth<960){
            setDropdown(false)
        }
        else{
            setDropdown(true)
        }
    }

    const onMouseLeave=( )=> {
        if(window.innerWidth<960){
            setDropdown(false)
        }
        else{
            setDropdown(false)
        }
    }

    return(
        <nav className='navbars'> 
            <Link to='/'
            className='navbar-logos'>
                Artisen
            </Link>


            <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
            </div>

            <ul className= {click ? 'nav-menu active' : 'nav-menu'}>
                <li className = 'nav-itema'>
                    <Link to='/' className='nav-links' onClick={closeMobilMenu}>
                    Inicio
                    </Link>
                </li>

                <li className = 'nav-itema' onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
                    <Link to='/servicios' className='nav-links' onClick={closeMobilMenu}>
                    Perfil <i className='fas fa-caret-down'/>
                    </Link>
                    {dropdown && <Dropdown />}
                </li>
                
                <li className = 'nav-itema'>
                    <Link to='/productos' className='nav-links' onClick={closeMobilMenu}>
                    Productos
                    </Link>
                </li>

                <li className = 'nav-itema'>
                    <Link to='/Contacto' className='nav-links' onClick={closeMobilMenu}>
                    Contacto
                    </Link>
                </li>


                <li className = 'nav-itema'>
                    <Link to='/login' className='nav-links-mobile' onClick={closeMobilMenu}>
                    Login
                    </Link>
                </li>

            </ul>
            <Button />
        </nav>

        
    );
}

export default Navbar;


