import React, {Fragment, useState} from 'react';
import ReactDOM from "react-dom";

export default function Servicios(){
    const [text, setText]= useState(
        window.localStorage.getItem("text")
    )

    const setLocalStorage =value =>{
        try{
            setText(value)
            window.localStorage.setItem("text",value)
        }
        catch(error){
            console.error(error)
        }
    }


    return(

    <Fragment>

<div class="container">




  <div class="bg-dark p-3 mt-4"> 
    <h1 class="text-center m-0 text-light">Artisen</h1>
    </div>

<div class="container-xl mt-3" >
    <div class="card text-center">
        <div class="card-header">
          Detalles de usuario
        </div>
        <div class="card-body">
            <div class="row g-2">
                
                <div class="col-sm-4 " >
                    <div class="d-flex justify-content-center">
                    <img src={"./NARUTO.jpg"} class="rounded-circle" width="200px" height="200px"/>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Usuario</li>
                    </ul>
                </div>
                <div class="col-sm-8" >
                    <ul class="list-group list-group-horizontal-xxl" >
                        <li class="list-group-item">ID: 01</li>
                        <li class="list-group-item">Correo: cesaradonis1@hotmail.com</li>
                        <li class="list-group-item">Rol: ADM</li>
                        <li class="list-group-item">Hola el equipo de Artisen te ama guap@.</li>

                      </ul>
                </div>
              </div>
        </div>
        <div class="card-footer text-muted">
            Fecha de creacion: Hoy
        </div>
      </div>
</div>





<div class=" mt-3"> 
    <h1 class="text-center m-0 ">Notas</h1>
    </div>


  <div class="row justify-content-center  border-bottom pb-2 pt-3">
                <div class="col-10">
        <textarea 
        onChange={e => setLocalStorage(e.target.value)}
        value={text}
        placeholder="¿Que esta pasando?" className="form-control mt-3 p-3" />

    <button className="btn btn-primary w-100 mt-2">Agregar nota</button>
 


    



    
    </div>



    </div>
 </div>
    </Fragment>



    );
}