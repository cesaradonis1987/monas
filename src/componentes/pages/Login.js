import React from 'react';
import './login.css';

export default function Login(){
    return(

        <>
        <body class="bg-dark body1">
    <section>
        <div class="row g-0">
            <div class="col-lg-7 d-none d-lg-block">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item img-1 min-vh-100 active">

                        </div>
                        <div class="carousel-item img-2 min-vh-100">

                        </div>
                        <div class="carousel-item img-3 min-vh-100">

                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <div class="col-lg-5 d-flex flex-column align-items-end min-vh-100">

                <div class="px-lg-5 py-lg-4 p-4 align-self-center w-100">
                    <h1 class="font-weight-bold mb-4">Bienvenido de vuelta</h1>
                    <form class="mb-3">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label font-weight-bold">Email</label>
                            <input type="email" class="form-control bg-dark-x border-0" id="exampleInputEmail1" placeholder="Ingresa tu email" aria-describedby="emailHelp"/>

                        </div>
                        <div class="mb-4">
                            <label for="exampleInputPassword1" class="form-label font-weight-bold">Password</label>
                            <input type="password" class="form-control bg-dark-x border-0 mb-1" placeholder="Ingresa tu contraseña" id="exampleInputPassword1" />
                            <a href="#" id="emailHelp" class="form-text text-decoration-none">¿Olvidaste tu contraseña?</a>
                        </div>

                        <button type="submit" class="btn btn-primary w-100">Iniciar sesion</button>
                    </form>

                    <p class="font-weight-bold text-center text-muted">O inicia sesion con</p>
                    <div class="d-flex justify-content-around">
                        <button type="submit" class="btn btn-outline-light flex-grow-1 m-1">Google</button>
                        <button type="submit" class="btn btn-outline-light flex-grow-1 m-1">Facebook</button>
                    </div>


                </div>

                <div class="text-center px-lg-5 pt-lg-3 pb-lg-4 p-4 w-100 mt-auto">
                    <p class="d-inline-block mb-0">¿Todavia no tienes una cuenta?</p> <a href="#" class="text-light font-weight-bold text-decoration-none">Crear una ahora</a>
                </div>

            
        </div>
        </div>
</section>
</body>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        </>
    );
}