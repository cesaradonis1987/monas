import React from 'react';
import './Fotter.css';

export default function Fotter(){
    return(
        <>
        <footer>
    <div class="footer">
        <div class="contain">
        <div class="col">
          <h1>Contacto</h1>
          <ul>
            <li></li>
            
            <li><i class="fas fa-phone-square-alt fa-2x"></i> +52 998 3997 088 <p>Numero de la empresa</p></li>
           
            <li><i class="fas fa-map-marker-alt fa-2x"></i> Carretera Cancún-Aeropuerto, Km. 11.5 S.M. 299, Mz. 5, Lt 1, 77565 Cancùn, Q.R.</li>
            <li style={{color:'#0e7ebd;'}}><i class="icon-envelope icon-2x" style={{color: 'white;'}}></i> cesaradonis1@hotmail.com</li>
          </ul>
        </div>
        
        
        
        
        <div class="col social">
          <h1>About company</h1>
          <ul>
            <li><p>Somos una empresa dedicada a la venta de accesorios y productos de indole "Anime" el cual se compromete a llevar productos de gran calidad a nuestros consumidores, ademas proporcionar lo mejor en calidad de manga, accesorios y cosplay.</p></li>
            <li><i class="fab fa-facebook-square fa-5x"></i> <i class="fab fa-twitter-square fa-5x"></i> <i class="fab fa-github-square fa-5x  "></i> <i class="fab fa-google-plus-square fa-5x"></i></li>
          </ul>
        </div>
      <div class="clearfix"></div>
      </div>
      </div>
</footer>
        </>
    );
    }


